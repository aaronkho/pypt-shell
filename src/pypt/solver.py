import copy
import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
from fipy import Grid1D, Grid2D, CellVariable, FaceVariable, Viewer
from fipy import ExplicitDiffusionTerm, DiffusionTerm, TransientTerm, ImplicitSourceTerm, ConvectionTerm, CentralDifferenceConvectionTerm, ExponentialConvectionTerm, HybridConvectionTerm, PowerLawConvectionTerm, UpwindConvectionTerm, VanLeerConvectionTerm, AdvectionTerm, FirstOrderAdvectionTerm

# TODO: You should add your custom functions into utils.py
from . import utils

class PlasmaTransportSolver():

    def __init__(self, **kwargs):

        self._t_scale = 1.0e3
        self._n_scale = 1.0e19
        self._q_scale = 1.0e0

        self._grid_points = None
        self._grid_spacing = None
        self._mesh = None

        self._minor_radius = None
        self._major_radius = None
        self._r_norm_face = None
        self._r_norm_cell = None
        self._v_prime = None

        self._internal_boundary = None
        self._input_electron_temperature = None
        self._input_ion_temperature = None
        self._input_electron_density = None
        self._input_safety_factor = None

        self._electron_heat_diffusion_fn = None
        self._ion_heat_diffusion_fn = None
        self._electron_heat_source_fn = None
        self._ion_heat_source_fn = None

        self._electron_temperature = None
        self._ion_temperature = None
        self._electron_density = None

        use_defaults = kwargs.get("default", False)
        if use_defaults:
            self.initialize_with_defaults(**kwargs)

    def initialize_with_defaults(self, **kwargs):

        kwargs["r_min"] = 1.0
        kwargs["R_maj"] = 3.0
        kwargs["n_grid"] = 51
        if "n_grid" in kwargs:
            self.initialize_grid(**kwargs)
            self.initialize_geometry(**kwargs)
        if self._r_norm_cell is not None:
            self.initialize_profiles(**kwargs)
            self.initialize_constraints(**kwargs)
            self.initialize_sources(**kwargs)
            self.initialize_coefficients(**kwargs)

    def initialize_grid(self, **kwargs):

        grid_points = kwargs.get("n_grid", None)
        if grid_points is not None:
            self._grid_points = grid_points

        if self._grid_points is None:
            raise ValueError("Valid number of grid points, r_min, must be provided!")

        self._grid_spacing = 1.0 / self._grid_points
        self._mesh = Grid1D(nx=self._grid_points, dx=self._grid_spacing)

    def initialize_geometry(self, **kwargs):

        if self._mesh is None:
            raise ValueError("Invalid or uninitialized grid!")

        self._r_norm_cell = self._mesh.cellCenters()[0]
        self._r_norm_face = self._mesh.faceCenters[0]
        self._vprime = 0.0   # TODO!!!

        minor_radius = kwargs.get("r_min", None)
        if minor_radius is not None:
            self._minor_radius = minor_radius
        major_radius = kwargs.get("R_maj", None)
        if major_radius is not None:
            self._major_radius = major_radius

        if self._minor_radius is not None and self._major_radius is not None:

            self._vprime *= 1.0   # TODO!!!

    def initialize_profiles(self, **kwargs):

        if self._input_electron_temperature is None:
            self.set_initial_electron_temperature_profile(**kwargs)
        if self._input_ion_temperature is None:
            self.set_initial_ion_temperature_profile(**kwargs)
        if self._input_electron_density is None:
            self.set_initial_electron_density_profile(**kwargs)

        self._electron_temperature = CellVariable(name="Electron Temperature", mesh=self._mesh, value=self._input_electron_temperature)
        self._ion_temperature = CellVariable(name="Ion Temperature", mesh=self._mesh, value=self._input_ion_temperature)
        self._electron_density = CellVariable(name="Electron Density", mesh=self._mesh, value=self._input_electron_density)

    def initialize_constraints(self, **kwargs):

        if self._electron_temperature is None:
            raise ValueError("Invalid or uninitialized profiles!")
        if self._ion_temperature is None:
            raise ValueError("Invalid or uninitialized profiles!")
        if self._electron_density is None:
            raise ValueError("Invalid or uninitialized profiles!")

        self._internal_boundary = kwargs.get("internal_boundary", 1.0)
        self._electron_temperature.constrain(self._internal_boundary, self._mesh.facesRight)
        self._ion_temperature.constrain(self._internal_boundary, self._mesh.facesRight)
        self._electron_density.constrain(self._internal_boundary, self._mesh.facesRight)

    def initialize_sources(self, **kwargs):

        self.set_electron_heat_source_function(**kwargs)
        self.set_ion_heat_source_function(**kwargs)

    def initialize_coefficients(self, **kwargs):

        self.set_electron_heat_diffusion_function(**kwargs)
        self.set_ion_heat_diffusion_function(**kwargs)

    def set_initial_electron_temperature_profile(self, T_e=utils.default_temperature_fn, **kwargs):

        if self._r_norm_cell is None:
            raise ValueError("Initialize grid before initializing profiles!")

        use_scale = kwargs.get("scale", True)
        scale = self._t_scale if use_scale else 1.0
        if callable(T_e):
            self._input_electron_temperature = T_e(self._r_norm_cell, **kwargs) / scale
        elif isinstance(T_e, np.ndarray) and T_e.shape == self._r_norm_cell.shape:
            self._input_electron_temperature = copy.deepcopy(T_e) / scale

    def set_initial_ion_temperature_profile(self, T_i=utils.default_temperature_fn, **kwargs):

        if self._r_norm_cell is None:
            raise ValueError("Initialize grid before initializing profiles!")

        use_scale = kwargs.get("scale", True)
        scale = self._t_scale if use_scale else 1.0
        if callable(T_i):
            self._input_ion_temperature = T_i(self._r_norm_cell, **kwargs) / scale
        elif isinstance(T_i, np.ndarray) and T_i.shape == self._r_norm_cell.shape:
            self._input_ion_temperature = copy.deepcopy(T_i) / scale

    def set_initial_electron_density_profile(self, n_e=utils.default_density_fn, **kwargs):

        if self._r_norm_cell is None:
            raise ValueError("Initialize grid before initializing profiles!")

        use_scale = kwargs.get("scale", True)
        scale = self._n_scale if use_scale else 1.0
        if callable(n_e):
            self._input_electron_density = n_e(self._r_norm_cell, **kwargs) / scale
        elif isinstance(n_e, np.ndarray) and n_e.shape == self._r_norm_cell.shape:
            self._input_electron_density = copy.deepcopy(n_e) / scale

    # TODO: You will need to code up and pass functions defining the transport coefficients
    def set_electron_heat_diffusion_function(self, chi_e=utils.ones_fn, **kwargs):
        if callable(chi_e):
            self._electron_heat_diffusion_fn = chi_e

    def set_ion_heat_diffusion_function(self, chi_i=utils.ones_fn, **kwargs):
        if callable(chi_i):
            self._ion_heat_diffusion_fn = chi_i

    # TODO: You will need to code up and pass functions defining the source terms
    def set_electron_heat_source_function(self, P_e=utils.zeros_fn, **kwargs):
        if callable(P_e):
            self._electron_heat_source_fn = P_e

    def set_ion_heat_source_function(self, P_i=utils.zeros_fn, **kwargs):
        if callable(P_i):
            self._ion_heat_source_fn = P_i

    # TODO!!!
    def step(self):

        dt = 0.0

        electron_heat_equation = None
        ion_heat_equation = None

        plasma_transport_system = electron_heat_equation & ion_heat_equation
        plasma_transport_system.solve(dt=dt)

        return dt

    def solve(self, tend):

        viewer = Viewer(
            vars=(self._electron_temperature, self._ion_temperature),
            datamin=0.0,
            datamax=1.2 * np.nanmax(np.concatenate([self._input_electron_temperature, self._input_ion_temperature]))
        )

        timenow = 0.0
        timetext = viewer.axes.text(0.01, 0.2, f't = {timenow:.4f}s')

        while timenow < tend:

            dt = self.step()
            timenow += dt

            timedata = f't = {timenow:.4f}s ;  dt = {dt:.4f}s'
            timetext.remove() # Remove old text to increment time
            timetext = viewer.axes.text(0.01, 0.2, timedata)

            viewer.plot()

        input("Final state. Press <return> to proceed...")
