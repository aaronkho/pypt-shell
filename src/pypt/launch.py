import os
import sys
import argparse
import json
from pathlib import Path

from . import solver

def parse_input():

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', dest='userjson', type=str, default=None, action='store', help='Path to JSON file containing user inputs')
    parser.add_argument('-v', dest='verbosity', default=0, action='count', help='Set level of verbosity')
    return parser.parse_args()

def make_kwargs(jsonfile=None, verbosity=0):

    kwargs = {'default': True}
    try:
        jsonpath = Path(jsonfile)
        with open(jsonpath, 'r') as jf:
            kwargs = json.loads(jf.read())
    except Exception as e:
        if verbosity > 1:
            print(repr(e))
        kwargs = {'default': True}
    return kwargs

def main():

    args = parse_input()
    kwargs = make_kwargs(args.userjson, verbosity=args.verbosity)
    if args.verbosity > 0:
        print("User keyword arguments = ", kwargs)
    model = solver.PlasmaTransportSolver(**kwargs)
    t_final = kwargs.get('time_end', 0.01)
    model.solve(t_final)

if __name__ == "__main__":
    main()
