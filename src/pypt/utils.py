import numpy as np

pi = np.pi
ee = 1.602e-19
mp = 1.67e-27
me = 9.11e-31
eps0 = 8.854e-12
mu0 = 1.2566e-6
eps = 1.0e-8    # Small number

def zeros_fn(x, **kwargs):
    return np.zeros(x.shape)

def ones_fn(x, **kwargs):
    return np.ones(x.shape)

def zeros_2d_fn(x, **kwargs):
    return np.atleast_2d(np.zeros(x.shape))

def ones_2d_fn(x, **kwargs):
    return np.atleast_2d(np.ones(x.shape))

def default_temperature_fn(x, **kwargs):
    core_val = kwargs.get("core_t", 3.0e3)
    ped_val = kwargs.get("ped_t", 2.0e2)
    ped_offset = kwargs.get("ped_offset_t", 0.95)
    ped_width = kwargs.get("ped_width_t", 0.05)
    core_scale = core_val - ped_val
    ped_scale = ped_val / 2.0
    t_core = core_scale * np.cos(x * pi / 2.0)
    t_ped = ped_scale * (1.0 - np.tanh(3.0 * (x - ped_offset) / ped_width))
    return (t_core + t_ped).reshape(x.shape)

def default_density_fn(x, **kwargs):
    core_val = kwargs.get("core_n", 5.0e19)
    ped_val = kwargs.get("ped_n", 3.0e19)
    ped_offset = kwargs.get("ped_offset_n", 0.95)
    ped_width = kwargs.get("ped_width_n", 0.05)
    core_scale = core_val - ped_val
    ped_scale = ped_val / 2.0
    n_core = core_scale * np.cos(x * pi / 2.0)
    n_ped = ped_scale * (1.0 - np.tanh(3.0 * (x - ped_offset) / ped_width))
    return (n_core + n_ped).reshape(x.shape)

# TODO: Add your custom functions below
